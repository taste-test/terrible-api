from rest_framework import serializers
from api.models import CollectedMedia
from api.models import Collection
from api.models import Image
from api.models import Media
from api.models import Stream
from api.models import Track
from django.contrib.auth.models import User


class StreamSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stream
        fields = ('id', 'url', 'site_type')


class TrackSerializer(serializers.ModelSerializer):
    class Meta:
        model = Track
        fields = ('id', 'path', 'media_format')


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ('id', 'path')

    path = serializers.ImageField(use_url="images")


class MediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Media
        fields = ('id', 'stream', 'track', 'image')

    stream = StreamSerializer(required=False,
                              read_only=True)
    track = TrackSerializer(required=False,
                            read_only=True)
    image = ImageSerializer(required=False,
                            read_only=True)


class CollectedMediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = CollectedMedia
        fields = ('id', 'index', 'media',
                  'title', 'description',
                  'stream_media_type',
                  'start', 'end', 'collection',
                  'stats', 'collected_comments',)

    def create(self, validated_data):
        collected_media = CollectedMedia.objects.create(**validated_data)
        return collected_media


class CollectedMediaGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = CollectedMedia
        fields = ('id', 'index', 'media',
                  'title', 'description',
                  'stream_media_type',
                  'start', 'end', 'collection',
                  'stats', 'collected_comments',)

    media = MediaSerializer(
        many=False,
        read_only=True,
    )

class CollectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Collection
        fields = (
            'id', 'owner', 'title',
            'description', 'created',
            'modified', 'randomized',
            'size', 'collectedmedia_set'
            # 'collection_comments',
        )

    collectedmedia_set = CollectedMediaGetSerializer(
        many=True,
        read_only=True,
        required=False,
    )
    owner = serializers.ReadOnlyField(source='owner.username')


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'id')
