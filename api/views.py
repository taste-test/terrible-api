from api.models import Collection
from api.models import CollectedMedia
from api.models import Media
from api.models import Stream
from api.models import Image
from api.models import Track
from api.serializers import MediaSerializer
from api.serializers import StreamSerializer
from api.serializers import ImageSerializer
from api.serializers import TrackSerializer
from api.serializers import CollectionSerializer
from api.serializers import CollectedMediaSerializer
from api.serializers import CollectedMediaGetSerializer
from api.serializers import UserSerializer
from django.contrib.auth.models import User
from rest_framework import viewsets


class CollectionViewSet(viewsets.ModelViewSet):
    queryset = Collection.objects.all()
    serializer_class = CollectionSerializer

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class CollectedMediaViewSet(viewsets.ModelViewSet):
    queryset = CollectedMedia.objects.all()
    serializer_class = CollectedMediaSerializer


class MediaViewSet(viewsets.ModelViewSet):
    queryset = Media.objects.all()
    serializer_class = MediaSerializer


class StreamViewSet(viewsets.ModelViewSet):
    queryset = Stream.objects.all()
    serializer_class = StreamSerializer


class ImageViewSet(viewsets.ModelViewSet):
    queryset = Image.objects.all()
    serializer_class = ImageSerializer


class TrackViewSet(viewsets.ModelViewSet):
    queryset = Track.objects.all()
    serializer_class = TrackSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
