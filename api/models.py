from django.db import models
import uuid


class Collection(models.Model):
    id = models.UUIDField(primary_key=True,
                          default=uuid.uuid4,
                          editable=False)
    owner = models.ForeignKey('auth.User',
                              on_delete=models.CASCADE)
    title = models.CharField(max_length=100,
                             blank=True,
                             default='')
    description = models.TextField(blank=True,
                                   default='')
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now_add=True)
    randomized = models.BooleanField(default=False)
    size = models.PositiveSmallIntegerField(blank=False,
                                            default=0)
    collection_comments = models.ManyToManyField(
        'auth.User',
        through='CollectionComment',
        through_fields=('collection',
                        'commenter'),
        related_name='collection_commenter')


class CollectedMedia(models.Model):
    index = models.PositiveSmallIntegerField(blank=True,
                                             default=0)
    collection = models.ForeignKey('Collection',
                                   on_delete=models.CASCADE)
    media = models.ForeignKey('Media',
                              on_delete=models.CASCADE)
    stats = models.ManyToManyField(
        'auth.User',
        through='Stat',
        through_fields=('collectedmedia', 'user'))
    collected_comments = models.ManyToManyField(
        'auth.User',
        through='Comment',
        through_fields=('collectedmedia', 'commenter'),
        related_name='commenter')
    title = models.CharField(max_length=100,
                             blank=True,
                             default='')
    description = models.TextField(blank=True,
                                   default='')
    STREAM_MEDIA_TYPES = (
        ('A', 'Audio'),
        ('AV', 'Video'),
        ('V', 'Visual'),
    )
    stream_media_type = models.CharField(
        max_length=2,
        choices=STREAM_MEDIA_TYPES,
        blank=True
    )
    start = models.DurationField(default='00:00:00')
    end = models.DurationField(default='00:00:00')
    average_rating = models.DecimalField(default=0,
                                         max_digits=3,
                                         decimal_places=2)
    play_count = models.PositiveSmallIntegerField(blank=False,
                                                  default=0)
    comment_count = models.PositiveSmallIntegerField(blank=False,
                                                     default=0)

    def save(self, *args, **kwargs):
        if self.index == 0 or self.index == "":
            self.index = self.collection.size + 1
            self.collection.size += 1
            self.collection.save()
        super(CollectedMedia, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        if self.collection.size > 0:
            self.collection.size -= 1
        else:
            self.collection.size == 0
        self.collection.save()
        super(CollectedMedia, self).delete(*args, **kwargs)

    class Meta:
        unique_together = (('collection', 'index'),)
        ordering = ['index']


class Media(models.Model):
    collection = models.ManyToManyField(
        Collection,
        through='CollectedMedia',
        through_fields=('media', 'collection'),
    )


class Stream(Media):
    url = models.URLField(max_length=240,
                          default='',
                          unique=True)
    SITE_TYPES = (
        ('sc', 'Soundcloud'),
        ('yt', 'YouTube'),
        ('bc', 'Bandcamp'),
        ('vm', 'Vimeo'),
    )
    site_type = models.CharField(max_length=2,
                                 choices=SITE_TYPES,
                                 default='sc')

    def delete(self, *args, **kwargs):
        collected_medias = CollectedMedia.objects.filter(media=self)
        for medias in collected_medias:
            medias.delete()
        super(Stream, self).delete(*args, **kwargs)


class Track(Media):
    path = models.FileField(upload_to="tracks",
                            default='',
                            unique=True)
    MEDIA_FORMATS = (
        ('mp4', 'mp4'),
        ('m4a', 'm4a'),
        ('mp3', 'mp3'),
        ('flac', 'flac'),
        ('ogg', 'ogg'),
        ('wav', 'wav'),
        ('opus', 'opus'),
    )
    media_format = models.CharField(max_length=4,
                                    choices=MEDIA_FORMATS,
                                    default='opus')

    def delete(self, *args, **kwargs):
        collected_medias = CollectedMedia.objects.filter(media=self)
        for medias in collected_medias:
            medias.delete()
        super(Track, self).delete(*args, **kwargs)


class Image(Media):
    path = models.ImageField(upload_to="images",
                             max_length=100,
                             default='',
                             unique=True)

    def delete(self, *args, **kwargs):
        collected_medias = CollectedMedia.objects.filter(media=self)
        for medias in collected_medias:
            medias.delete()
        super(Image, self).delete(*args, **kwargs)


class Stat(models.Model):
    RATING_CHOICES = (
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
    )
    rating = models.PositiveSmallIntegerField(choices=RATING_CHOICES)
    created = models.DateTimeField(auto_now_add=True)
    played = models.BooleanField(default=False)
    play_date = models.DateTimeField(blank=True)
    user = models.ForeignKey('auth.User',
                             on_delete=models.CASCADE)
    collectedmedia = models.ForeignKey('CollectedMedia',
                                       on_delete=models.CASCADE)

    class Meta:
        unique_together = (('user', 'collectedmedia'),)


class Comment(models.Model):
    comment = models.TextField(blank=False)
    created = models.DateTimeField(auto_now_add=True)
    commenter = models.ForeignKey('auth.User',
                                  on_delete=models.CASCADE)
    collectedmedia = models.ForeignKey('CollectedMedia',
                                       on_delete=models.CASCADE)


class CollectionComment(models.Model):
    comment = models.TextField(blank=False)
    created = models.DateTimeField(auto_now_add=True)
    commenter = models.ForeignKey('auth.User',
                                  on_delete=models.CASCADE)
    collection = models.ForeignKey('Collection',
                                   on_delete=models.CASCADE)
