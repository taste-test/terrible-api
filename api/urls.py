from django.conf.urls import url, include
from api import views
from rest_framework.routers import DefaultRouter

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'media', views.MediaViewSet)
router.register(r'streams', views.StreamViewSet)
router.register(r'tracks', views.TrackViewSet)
router.register(r'images', views.ImageViewSet)
router.register(r'collections', views.CollectionViewSet)
router.register(r'collectedmedia', views.CollectedMediaViewSet)
router.register(r'users', views.UserViewSet)

# The API URLs are now determined automatically by the router.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls',
                               namespace='rest_framework'))
]
